import React from 'react';

const Step3 = ({ dishes, setDishes, dishOptions, errors, handleNavigate }) => {
  const selectedDishes = Object.keys(dishes);

  const addDishToSelected = (dish) => {
    setDishes((prevProps) => ({...prevProps, [dish]: 1}));
  }
  const updateDishCount = (dish, count) => {
    setDishes((prevProps) => ({...prevProps, [dish]: count}));
  }

  const renderDishSelector = (dish = "", index = -1) => {
    const selectorOptions = dish ? dishOptions.filter((dish) => !selectedDishes.includes(dish)).concat([dish]) : dishOptions.filter((dish) => !selectedDishes.includes(dish));
    return (
      <React.Fragment key={dish}>
        <div className="step3__input">
          <select value={dish} onChange={(e) => addDishToSelected(e.target.value)}>
            <option value="" disabled>---</option>
            { selectorOptions.map((dish) => <option key={dish}>{ dish }</option>)}
          </select>
        </div>
        <div className="step3__input">
          <input type="number" value={dishes[dish]} onChange={(e)=>updateDishCount(dish, parseInt(e.target.value) || "" )}/>
        </div>
      </React.Fragment>
    )
  }

  return (
    <div className="form step3">

      <div className="step3__inputs">
        <label>Please Select a Dish</label>
        <label>Please enter no. of servings</label>

        { errors["dishes"] && <span className="error">{ errors["dishes"] }</span> } 
        
        { Object.keys(dishes).map((dish, index) => renderDishSelector(dish, index) )}
        { Object.keys(dishes).length < dishOptions.length && renderDishSelector() }
      </div>

      <div className="form__navigation">
        <button onClick={() => handleNavigate("Step 2")}>Previous</button>
        <button onClick={() => handleNavigate("Review")}>Next</button>
      </div>

    </div>
  )
}

export default Step3;