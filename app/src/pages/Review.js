import React from 'react';
import '../styles/Review.scss';

const Review = ({ meal, numPeople, restaurant, dishes, handleNavigate }) => {

  const handleSubmit = () => {
    console.log(`Meal: ${meal}`)
    console.log(`No. of People ${numPeople}`)
    console.log(`Restaurant: ${restaurant}`)
    console.log(`Dishes:`)
    Object.entries(dishes).forEach(([dishName, dishQty], index) => {
      console.log(`   ${index + 1}: ${dishName} - ${dishQty} portions`);
    });
  }

  return (
    <div className="review">

      <div className="review__table">

        <div className="review__heading">Meal</div>
        <div className="review__meal">{ meal }</div>
        <div className="review__heading">No. of People</div>
        <div className="review__numPeople">{ numPeople }</div>
        <div className="review__heading">Restaurant</div>
        <div className="review__restaurant">{ restaurant }</div>

        <div className="review__heading">Dishes</div>
        <div className="review__dishes">
          { Object.entries(dishes).map(([dishName, dishQty]) =>
            <React.Fragment key={dishName}>
              <div className="review__dishName" key={dishName}>{ dishName }</div>
              <div className="review__dishQty" key={`${dishName}-qty`}>{ dishQty }</div>
            </React.Fragment>
          )}
        </div>
      </div>  

      <div className="form__navigation">
        <button onClick={() => handleNavigate("Step 3")}>Previous</button>
        <button onClick={handleSubmit}>Submit</button>
      </div>

    </div>
  )
}

export default Review;