import React from 'react';

const Step1 = ({ meal, setMeal, numPeople, setNumPeople, handleNavigate, errors }) => {
  return (
    <div className="form step1">

      <label>
        Please Select a meal 
        { errors["meal"] && <span className="error">{ errors["meal"] }</span> }
      </label>
      <select value={meal} onChange={(e) => setMeal(e.target.value)}>
        <option value="" disabled>---</option>
        <option value="breakfast">Breakfast</option>
        <option value="lunch">Lunch</option>
        <option value="dinner">Dinner</option>
      </select>

      <label>
        Please Enter Number of people
        { errors["numPeople"] && <span className="error">{ errors["numPeople"] }</span> } 
      </label>
      <input type="number" min="1" max="10" value={numPeople} onChange={(e) => setNumPeople(e.target.value)}/>

      <div className="form__navigation">
        <div />
        <button onClick={(e)=>handleNavigate("Step 2")}>Next</button>
      </div>

    </div>
  )
}

export default Step1;