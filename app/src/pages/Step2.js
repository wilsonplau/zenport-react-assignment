import React from 'react';

const Step2 = ({ restaurant, setRestaurant, restaurantOptions, errors, handleNavigate }) => {
  return (
    <div className="form step2">

      <label>
        Please Select a Restaurant
        { errors["restaurant"] && <span className="error">{ errors["restaurant"] }</span> } 
      </label>
      <select value={restaurant} onChange={(e)=>setRestaurant(e.target.value)}>
          <option value="" disabled>---</option>
        { restaurantOptions.map((restaurant) => 
          <option key={restaurant} value={restaurant}>
            { restaurant }
          </option>
        )}
      </select>

      <div className="form__navigation">
        <button onClick={(e)=>handleNavigate("Step 1")}>Previous</button>
        <button onClick={(e)=>handleNavigate("Step 3")}>Next</button>
      </div>

    </div>
  )
}

export default Step2; 