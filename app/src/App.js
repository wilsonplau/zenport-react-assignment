import React, { useState } from 'react';
import './styles/Reset.scss';
import './styles/App.scss';
import './styles/Form.scss';
import NavigationBar from './components/NavigationBar.js';
import Step1 from './pages/Step1.js';
import Step2 from './pages/Step2.js';
import Step3 from './pages/Step3.js';
import Review from './pages/Review.js';
import useForm from './hooks/useForm.js';

const App = () => {
  const {
    meal, setMeal,
    numPeople, setNumPeople,
    restaurant, setRestaurant, restaurantOptions,
    dishes, setDishes, dishOptions,
    errors, validateForm
  } = useForm();

  const [step, setStep] = useState("Step 1");
  const handleNavigate = (stepTo) => {
    const valid = validateForm(stepTo);
    if (valid) setStep(stepTo);
  }

  return (
    <div className="app">
      <NavigationBar step={step} handleNavigate={handleNavigate} />

      { step === "Step 1" && <Step1 
        meal={meal} setMeal={setMeal} numPeople={numPeople} setNumPeople={setNumPeople} 
        handleNavigate={handleNavigate} errors={errors} 
      /> }
      
      { step === "Step 2" && <Step2 
        restaurant={restaurant} setRestaurant={setRestaurant} restaurantOptions={restaurantOptions} 
        handleNavigate={handleNavigate} errors={errors} 
      /> }

      { step === "Step 3" && <Step3 
        dishes={dishes} setDishes={setDishes} dishOptions={dishOptions}
        handleNavigate={handleNavigate} errors={errors} 
      /> }

      { step === "Review" && <Review 
        meal={meal} numPeople={numPeople} restaurant={restaurant} dishes={dishes}
        handleNavigate={handleNavigate}
      /> }

    </div>
  );
}

export default App;
