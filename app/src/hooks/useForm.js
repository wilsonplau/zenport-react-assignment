import { useState, useEffect } from 'react';
import data from '../data/dishes.json';

const useForm = () => {
  const [meal, setMeal] = useState("");
  const [numPeople, setNumPeople] = useState(1);
  const [restaurant, setRestaurant] = useState("");
  const [dishes, setDishes] = useState({});
  const [errors, setErrors] = useState({});

  const [restaurantOptions, setRestaurantOptions] = useState([]);
  useEffect(() => {
    const filteredRestaurants = new Set(data.dishes.filter((dish) => dish.availableMeals.includes(meal)).map((dish) => dish.restaurant));
    setRestaurantOptions(Array.from(filteredRestaurants));
    if (!filteredRestaurants.has(restaurant)) setRestaurant("");
  }, [meal])

  const [dishOptions, setDishOptions] = useState([]);
  useEffect(() => {
    const filteredDishes = data.dishes.filter((dish) => dish.availableMeals.includes(meal) && dish.restaurant === restaurant).map((dish) => dish.name);
    setDishOptions(filteredDishes);
    setDishes({});
  }, [meal, restaurant]);

  const validateForm = (step) => {
    const errorState = {};
    if (step === "Step 2" && !meal) 
      errorState["meal"] = "* This field is required.";
    if (step === "Step 2" && (numPeople < 1 || numPeople > 10))
      errorState["numPeople"] = "* This value must be between 1 and 10.";
    if (step === "Step 3" && !restaurant) 
      errorState["restaurant"] = "* This field is required."
    if (step === "Review" && !Object.values(dishes).reduce((acc, val) => (acc && val >= 0 && val < 11), true))
      errorState["dishes"] = "* Number of servings must be between 0 and 10"
    if (step === "Review" && Object.values(dishes).reduce((acc, val) => acc + val, 0) < numPeople) 
      errorState["dishes"] = "* Please add more dishes for your guests."
    setErrors(errorState);
    return Object.keys(errorState).length > 0 ? false : true;
  }

  return {
    meal, setMeal,
    numPeople, setNumPeople,
    restaurant, setRestaurant, restaurantOptions,
    dishes, setDishes, dishOptions,
    errors, validateForm
  }

}

export default useForm;