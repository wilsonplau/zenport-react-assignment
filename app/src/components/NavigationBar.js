import React from 'react';
import '../styles/NavigationBar.scss';

const NavigationBar = ({ step, handleNavigate }) => {
  const renderNavigationBarItem = (itemStep) => (
    <button 
      key={itemStep}
      className="navigationBar__item" 
      aria-pressed={step === itemStep}
      aria-disabled={false}
      onClick={() => handleNavigate(itemStep)}
    >
      { itemStep }
    </button>
  )

  const steps = ["Step 1", "Step 2", "Step 3", "Review"];
  return (
    <div className="navigationBar">
      { steps.map((step) => renderNavigationBarItem(step)) }
    </div>
  );
}

export default NavigationBar;