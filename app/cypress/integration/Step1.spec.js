describe('Step 1', () => {

  it('Should render two inputs when loaded with appropriate values', () => {
    cy.visit('http://localhost:3000')
    cy.get('select')
      .should('have.value', null)
      .select('Breakfast').should('have.value', 'breakfast')
      .select('Lunch').should('have.value', 'lunch')
      .select('Dinner').should('have.value', 'dinner')
    cy.get('input[type="number"]').should('have.value', "1")
  });

  it('Should error if nothing is selected for meal, and next is pressed', () => {
    cy.visit('http://localhost:3000')
    cy.contains('Next').click()
    cy.get('.error').should('contain', 'This field is required')
  })

  it('Should error if a value is too high or too low for number, and next is pressed', () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type('100');
    cy.contains('Next').click()
    cy.get('.error').should('contain', 'This value must be between 1 and 10')
    cy.get('input[type="number"]').clear().type('-1');
    cy.contains('Next').click()
    cy.get('.error').should('contain', 'This value must be between 1 and 10')
  })

  it('Should render the next page if the inputs are valid, and next is pressed', () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Lunch')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.get('.step2')
  })
  
})