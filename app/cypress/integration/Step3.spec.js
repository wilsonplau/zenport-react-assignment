describe('Step 3', () => {

  it('Should render a list with dish options depending on meal and restaurant (Combination #1)', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Egg Muffin')
    cy.get('select').first().should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('5');
  });

  it('Should render a list with dish options depending on meal and restaurant (Combination #2)', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Lunch')
    cy.contains('Next').click()
    cy.get('select').select('Taco Bell')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Tacos')
    cy.get('.step3__inputs .step3__input').first().find('select').should('have.value', 'Tacos')
    cy.get('.step3__inputs .step3__input').eq(2).find('select').select('Burrito')
    cy.get('.step3__inputs .step3__input').eq(2).find('select').should('have.value', 'Burrito')
    cy.get('.step3__inputs .step3__input').eq(3).find('input[type="number"]').clear().type('3');
    cy.get('.step3__inputs .step3__input').eq(4).find('select').select('Quesadilla')
    cy.get('.step3__inputs .step3__input').eq(4).find('select').should('have.value', 'Quesadilla')
    cy.get('.step3__inputs .step3__input').eq(5).find('input[type="number"]').clear().type('5');
  });

  it('Should render a list with dish options depending on meal and restaurant (Combination #3)', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Dinner')
    cy.contains('Next').click()
    cy.get('select').select('Olive Garden')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Garlic Bread')
    cy.get('.step3__inputs .step3__input').first().find('select').should('have.value', 'Garlic Bread')
    cy.get('.step3__inputs .step3__input').eq(2).find('select').select('Ravioli')
    cy.get('.step3__inputs .step3__input').eq(2).find('select').should('have.value', 'Ravioli')
    cy.get('.step3__inputs .step3__input').eq(3).find('input[type="number"]').clear().type('2');
    cy.get('.step3__inputs .step3__input').eq(4).find('select').select('Rigatoni Spaghetti')
    cy.get('.step3__inputs .step3__input').eq(4).find('select').should('have.value', 'Rigatoni Spaghetti')
    cy.get('.step3__inputs .step3__input').eq(5).find('input[type="number"]').clear().type('4');
    cy.get('.step3__inputs .step3__input').eq(6).find('select').select('Fettucine Pasta')
    cy.get('.step3__inputs .step3__input').eq(6).find('select').should('have.value', 'Fettucine Pasta')
    cy.get('.step3__inputs .step3__input').eq(7).find('input[type="number"]').clear().type('6');
  });

  it('Should error when not enough meals are selected and next is pressed', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').should('have.value', null).select('Egg Muffin')
    cy.get('select').should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('3');
    cy.contains('Next').click()
    cy.get('.error').should('contain', 'Please add more dishes for your guests')
  });

  it('Should error when number of meals is > 10', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').should('have.value', null).select('Egg Muffin')
    cy.get('select').should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('11');
    cy.contains('Next').click()
    cy.get('.error').should('contain', 'Number of servings must be between 0 and 10')
  });

  it("Should be able to navigate to the previous step with previous button", () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('.step3')
    cy.contains('Previous').click()
    cy.get('.step2')
  })

});