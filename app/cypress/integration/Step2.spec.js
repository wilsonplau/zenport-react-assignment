describe('Step 2', () => {

  it('Should render a list of breakfast restaurants if breakfast was selected', () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select')
      .should('have.value', null)
      .select("Mc Donalds").should('have.value', "Mc Donalds")
      .select("Vege Deli").should('have.value', "Vege Deli")
      .select("Olive Garden").should('have.value', "Olive Garden")
  });

  it('Should render a list of lunch restaurants if lunch was selected', () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Lunch')
    cy.contains('Next').click()
    cy.get('select')
      .should('have.value', null)
      .select("Mc Donalds").should('have.value', "Mc Donalds")
      .select("Taco Bell").should('have.value', "Taco Bell")
      .select("Vege Deli").should('have.value', "Vege Deli")
      .select("Pizzeria").should('have.value', "Pizzeria")
      .select("Panda Express").should('have.value', "Panda Express")
      .select("Olive Garden").should('have.value', "Olive Garden")
  });

  it('Should render a list of dinner restaurants if dinner was selected', () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Dinner')
    cy.contains('Next').click()
    cy.get('select')
      .should('have.value', null)
      .select("Mc Donalds").should('have.value', "Mc Donalds")
      .select("Taco Bell").should('have.value', "Taco Bell")
      .select("BBQ Hut").should('have.value', "BBQ Hut")
      .select("Vege Deli").should('have.value', "Vege Deli")
      .select("Pizzeria").should('have.value', "Pizzeria")
      .select("Panda Express").should('have.value', "Panda Express")
      .select("Olive Garden").should('have.value', "Olive Garden")
  });

  it("Should error when no restaurant is selected", () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Lunch')
    cy.contains('Next').click()
    cy.contains('Next').click()
    cy.get('.error').should('contain', "This field is required")
  })

  it("Should move to the next step when valid inputs are made", () => {
    cy.visit('http://localhost:3000')
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select').select("Mc Donalds")
    cy.contains('Next').click()
    cy.get('.step3')
  })

  it("Should be able to navigate to the previous step with previous button", () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('.step2')
    cy.contains('Previous').click()
    cy.get('.step1')
  })

});