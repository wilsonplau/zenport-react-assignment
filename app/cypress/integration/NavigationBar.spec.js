describe('NavigationBar', () => {
  it('Should not be able to navigate to Step 2, Step 3, or Review without valid input', () => {
    cy.visit('http://localhost:3000') 
    cy.contains('Step 2').click()
    cy.get('.step1')
    cy.contains('Step 3').click()
    cy.get('.step1')
  });  

  it('Should not be able to navigate to Step 3 or Review without valid input, but can go back', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.contains('Step 3').click()
    cy.get('.step2')
    cy.contains('Review').click()
    cy.get('.step2')
    cy.contains('Step 1').click()
    cy.get('.step1')
    cy.contains('Step 2').click()
    cy.get('.step2')
  });  

  it('Should not be able to navigate to Review without valid input, but can go back and forth', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.contains('Review').click()
    cy.get('.step3')
    cy.contains('Step 2').click()
    cy.get('.step2')
    cy.contains('Step 3').click()
    cy.get('.step3')
    cy.contains('Step 1').click()
    cy.get('.step1')
    cy.contains('Step 3').click()
    cy.get('.step3')
  });  

  it('Should be able to navigate back and forth between all pages with valid input', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').select('Egg Muffin')
    cy.contains('Next').click()
    cy.contains('Step 1').click()
    cy.get('.step1')
    cy.contains('Review').click()
    cy.get('.review')
    cy.contains('Step 2').click()
    cy.get('.step2')
    cy.contains('Review').click()
    cy.get('.review')
    cy.contains('Step 3').click()
    cy.get('.step3')
    cy.contains('Review').click()
    cy.get('.review')
  });  


});