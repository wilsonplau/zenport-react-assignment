describe('Review', () => {

  it('Should render an overview of all of the choices', () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type(2);
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Egg Muffin')
    cy.get('select').first().should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.get('.review__meal').should('contain', "breakfast")
    cy.get('.review__numPeople').should('contain', "2")
    cy.get('.review__restaurant').should('contain', "Mc Donalds")
    cy.get('.review__dishes').should('contain', "Egg Muffin")
    cy.get('.review__dishes').should('contain', "5")
  });

  it("Should be able to navigate to the previous step with previous button", () => {
    cy.visit('http://localhost:3000') 
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type(2);
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Egg Muffin')
    cy.get('select').first().should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.get('.review')
    cy.contains('Previous').click()
    cy.get('.step3')
  })

  it("Should console log results after submit is pressed", () => {
    cy.visit('http://localhost:3000') 
    let consoleLogSpy;
    cy.window().then((win) => {  
      consoleLogSpy = cy.spy(win.console, "log") 
    })
    cy.get('select').select('Breakfast')
    cy.get('input[type="number"]').clear().type(2);
    cy.contains('Next').click()
    cy.get('select').select('Mc Donalds')
    cy.contains('Next').click()
    cy.get('select').first().should('have.value', null).select('Egg Muffin')
    cy.get('select').first().should('have.value', 'Egg Muffin')
    cy.get('input[type="number"]').clear().type('5');
    cy.contains('Next').click()
    cy.contains('Submit').click().then(() => {
      expect(consoleLogSpy.called).to.be.true
    })
  })

})